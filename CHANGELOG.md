
## 0.0.11 [07-12-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/ned-validator!4

2024-07-12 19:45:01 +0000

---

## 0.0.10 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ned-validator!3

---

## 0.0.9 [06-28-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/ned-validator!2

---

## 0.0.8 [01-05-2022]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/ned-validator!1

---

## 0.0.7 [11-15-2021]

* Bug fixes and performance improvements

See commit 4b090b9

---

## 0.0.6 [11-11-2021]

* Bug fixes and performance improvements

See commit 7ace2dd

---

## 0.0.5 [11-11-2021]

* Bug fixes and performance improvements

See commit 5e584e9

---

## 0.0.4 [11-11-2021]

* Bug fixes and performance improvements

See commit 0b78c23

---

## 0.0.3 [11-11-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [11-10-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
