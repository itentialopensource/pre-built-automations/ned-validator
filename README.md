<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 05-02-2024 and will be end of life on 05-02-2025. The capabilities of this Pre-Built have been replaced by the [Cisco - NSO](https://gitlab.com/itentialopensource/pre-built-automations/cisco-nso)

<!-- Update the below line with your Pre-Built name -->
# Pre-Built Name

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview


The NED Validator accepts device configurations for various supported NEDs that are part of Cisco NSO (Network Services Orchestration). You can verify a configuration using the NED Validator before applying the configuration to your network devices.

_Estimated Run Time_: 1 minute

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2022.1`

## Requirements

This Pre-Built requires the following:

* @itential/adapter-nso

## Features

The main benefits and features of the Pre-Built are outlined below.

* Validates commands on selected adapters with a NED
* Displays results of commands

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

Run the NED Validator workflow from the workflow canvas or with a child job task. A manual task will display a form which will allow you to select the adapter and commands

Click CONFIRM and another form will display the adapter, commands, and results.

From here you can select TRY AGAIN to be prompted with another form and try different adapters of commands. Select END to finish the job.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
